#!/usr/bin/env python3

import json
import csv
import argparse
import gitlab
from datetime import datetime
from mako.template import Template

class Group_Member_Reporter():

    gitlab_url = "https://gitlab.com/"
    report_group = None
    all = False
    gl = None
    self_managed = False
    groups = []
    projects = []
    project_map = {}
    group_map = {}
    group_share_map = {}

    def __init__(self, args):
        if args.gitlaburl:
            self.gitlab_url = args.gitlaburl if args.gitlaburl.endswith("/") else args.gitlaburl + "/"
        self.all = args.all
        if self.gitlab_url != "https://gitlab.com/":
            self.self_managed = True
            if args.group:
                print("Discarding requested group, self-managed mode only supports all groups")
        else:
            if not args.group:
                print("ERROR: need to specify a group to query for https://gitlab.com/")
                exit()
            else:
                self.report_group = args.group
            if args.all:
                print("Report on all users not supported on SaaS, will only show billable users.")
        self.gl = gitlab.Gitlab(self.gitlab_url, private_token=args.token, retry_transient_errors=True)
        #verify user and API connection
        top_groups = []
        if self.self_managed:
            self.verify_sm_user()
            try:
                top_groups = self.gl.groups.list(iterator=True, top_level_only=True)
            except Exception as e:
                print("ERROR: Could not retrieve top level groups")
                print(e)
                exit()
            for top_group in top_groups:
                self.groups.append(top_group)
                self.get_subgroups(top_group)
                self.group_map = {group.id : group for group in self.groups}
            self.projects = self.get_projects()
            self.project_map = {project.id : project for project in self.projects}
        else:
            try:
                self.report_group = self.gl.groups.get(self.report_group)
                if "/" in self.report_group.full_path:
                    print("WARN: Subgroup report not supported, defaulting to top-level group %s instead" % self.report_group.full_path[0:self.report_group.full_path.find("/")])
                    self.report_group = self.gl.groups.get(self.report_group.full_path[0:self.report_group.full_path.find("/")])
                self.verify_saas_user()
            except Exception as e:
                print("ERROR: Could not retrieve group %s" % self.report_group)
                print(e)
                exit()
            self.groups = [self.report_group]
            self.get_subgroups(self.report_group)
            self.projects = self.get_projects(self.report_group)
        self.make_group_share_map()
        
    def get_subgroups(self, group):
        print("Getting subgroups for group %s" % group.full_path)
        subgroups = group.subgroups.list(iterator=True)
        for subgroup in subgroups:
            subgroup_object = self.gl.groups.get(subgroup.id)
            self.groups.append(subgroup_object)
            self.get_subgroups(subgroup_object)

    def get_projects(self, group=None):
        projects = []
        if group:
            print("Getting projects for group %s" % group.full_path)
            group_projects = group.projects.list(include_subgroups=True, iterator=True)
            for project in group_projects:
                projects.append(self.gl.projects.get(project.id))
        else:
            print("Getting all projects")
            project_objects = self.gl.projects.list(iterator=True, archived=False)
            for project in project_objects:
                projects.append(project)
        return projects
    
    def make_group_share_map(self):
        for project in self.projects:
            for shared_group in project.shared_with_groups:
                share = {"id":project.id, "type":"project", "membership_type":"shared", "path":project.path_with_namespace, "url":project.web_url + "/-/project_members", "access_level": shared_group["group_access_level"], "shared_via":shared_group["group_id"], "shared_path":shared_group["group_full_path"], "shared_url":self.gitlab_url+"/groups/"+shared_group["group_full_path"]+"/-/group_members"}
                if shared_group["group_id"] in self.group_share_map:
                    self.group_share_map[shared_group["group_id"]].append(share)
                else:
                    self.group_share_map[shared_group["group_id"]] = [share]
        for group in self.groups:
            if "shared_with_groups" in group.attributes:
                for shared_group in group.shared_with_groups:
                    share = {"id":group.id, "type":"group", "membership_type":"shared", "path":group.full_path, "url":group.web_url + "/-/group_members", "access_level": shared_group["group_access_level"], "shared_via":shared_group["group_id"], "shared_path":shared_group["group_full_path"], "shared_url":self.gitlab_url+"/groups/"+shared_group["group_full_path"]+"/-/group_members"}
                    if shared_group["group_id"] in self.group_share_map:
                        self.group_share_map[shared_group["group_id"]].append(share)
                    else:
                        self.group_share_map[shared_group["group_id"]] = [share]
    
    def get_users_memberships(self):
        print("Getting user memberships")
        users = None
        if self.self_managed:
            users = self.gl.users.list(iterator=True)
        else:
            users = self.report_group.billable_members.list(iterator=True)
        userlist = []
        for user in users:
            user_object = user.attributes
            if self.self_managed:
                user_object["last_login_at"] = user_object["last_sign_in_at"]
                if not self.all and user_object["using_license_seat"] == False:
                    continue
            else:
                user_object["using_license_seat"] = True
            memberships = [membership.attributes for membership in user.memberships.list(iterator=True)]
            user_object["memberships"] = memberships
            userlist.append(user_object)
        return userlist
    
    def consolidate_memberships(self, users):
        for user in users:
            result_memberships = []
            user["highest_access_level"] = 0
            for membership in user["memberships"]:
                # user is member in a group that has been shared to other groups / projects. Add this shared access to their memberships
                if self.self_managed:
                    membership = self.enhance_sm_membership(membership)
                source_type = "project" if membership["source_members_url"].endswith("/project_members") else "group"
                if membership["access_level"]["integer_value"] > user["highest_access_level"]:
                    user["highest_access_level"] = membership["access_level"]["integer_value"]
                result_membership = {"id":membership["source_id"], "type":source_type, "membership_type":"direct", "path":membership["source_full_name"].lower().replace(" ",""), "url":membership["source_members_url"], "access_level": membership["access_level"]["integer_value"]}
                result_memberships.append(result_membership)
                if membership["source_id"] in self.group_share_map:
                    shared = self.group_share_map[membership["source_id"]]
                    result_memberships.extend(shared)
            result_memberships = sorted(result_memberships, key=lambda d: (d['path'],d['access_level']), reverse=True) 
            user["memberships"] = result_memberships
        return users
    
    #align result of SM user memberships API with SaaS billable users API
    def enhance_sm_membership(self, membership):
        enhanced_membership = membership.copy()
        enhanced_membership["access_level"] = {"integer_value": membership["access_level"]}
        if membership["source_type"] == "Project":
            enhanced_membership["source_members_url"] = self.project_map[membership["source_id"]].web_url + "/-/project_members"
            enhanced_membership["source_full_name"] = self.project_map[membership["source_id"]].path_with_namespace
        else:
            enhanced_membership["source_members_url"] = self.group_map[membership["source_id"]].web_url + "/-/group_members"
            enhanced_membership["source_full_name"] = self.group_map[membership["source_id"]].full_path
        return enhanced_membership

    def write_csv(self, users):
        reportdate = datetime.now().strftime('%Y-%m-%d')
        filename = "user_report_" + reportdate + ".csv"
        with open("public/" + filename, "w") as user_report:
            writer = csv.writer(user_report)
            fields = ["username","name","email","highest_access_level","last_activity_on","last_login_at","state","using_license_seat"]
            header = fields.copy()
            header.extend(["member_of","access_level","type","source"])
            writer.writerow(header)
            for user in users:
                row = []
                for field in fields:
                    row.append(user[field])
                if not user["memberships"]:
                    row.extend(["","","",""])
                    writer.writerow(row)
                    continue
                for membership in user["memberships"]:
                    membership_row = row.copy()
                    membership_row.append(membership["path"])
                    membership_row.append(membership["access_level"])
                    membership_row.append(membership["membership_type"])
                    if membership["membership_type"] == "shared":
                        membership_row.append(membership["shared_path"])
                    else:
                        membership_row.append(membership["path"])
                    writer.writerow(membership_row)
    
    def write_json(self, users):
        reportdate = datetime.now().strftime('%Y-%m-%d')
        filename = "user_report_" + reportdate + ".json"
        with open("public/" + filename, "w") as user_report:
            json.dump(users, user_report)

    def write_html(self, userlist):
        mytemplate = Template(filename='template/index.html')
        report_group = ""
        if self.report_group:
            report_group = self.report_group.full_path
        with open("public/index.html","w") as outfile:
            outfile.write(mytemplate.render(users = userlist, gitlab = reporter.gitlab_url, group=report_group, reportdate = datetime.now().strftime('%Y-%m-%d')))

    def verify_sm_user(self):
        try:
            # fail early by not retrying transient errors, which shouldn't be necessary
            self.gl.auth()
            currentuser = self.gl.user
            if not currentuser.attributes.get("is_admin"):
                print("Error: Token does not have Admin credentials. Stopping.")
                exit()
        except Exception as e:
            print("Error: Can not evaluate token user. Stopping.")
            print(e)
            exit()

    def verify_saas_user(self):
        try:
            # fail early by not retrying transient errors, which shouldn't be necessary
            self.gl.auth()
            currentuser = self.gl.user
            reportgroup_members = self.report_group.members.list(iterator=True)
            is_owner = False
            for member in reportgroup_members:
                if member.id == currentuser.id:
                    is_owner = True
            if not is_owner:
                print("Error: Token for user %s does not belong to group Owner of %s. Stopping." % (currentuser.username, self.report_group.full_path))
                exit()
        except Exception as e:
            print("Error: Can not evaluate token user. Stopping.")
            print(e)
            exit()

parser = argparse.ArgumentParser(description='Create report for GitLab group members')
parser.add_argument('-u','--gitlaburl', help="Optional instance URL, default to https://gitlab.com/", default="https://gitlab.com/")
parser.add_argument('-g','--group', help="Group to report on. Not needed for self-managed.")
parser.add_argument('--all', help="Show all users for self-managed, otherwise report is limited to billable users", action='store_true')
parser.add_argument('token', help="Token with API scope able to read all groups to be reported on. Either admin or group owner token.")
args = parser.parse_args()

reporter = Group_Member_Reporter(args)

userlist = reporter.get_users_memberships()
userlist = reporter.consolidate_memberships(userlist)
userlist = sorted(userlist, key=lambda d: d['username']) 
reporter.write_csv(userlist)
reporter.write_json(userlist)
reporter.write_html(userlist)
